import {useParams} from 'react-router-dom'
import {useSelector} from 'react-redux'
import '../../css/index.css'

function DoneTodoDetail() {
    const {id} = useParams()
    const todo = useSelector(state => state.todo.todoList).find(todo => Number(todo.id) === Number(id))
    return (
        <div className="flexMain">
            id: {id}
            <br/>
            todo: {todo?.text}
        </div>
    )
}

export default DoneTodoDetail