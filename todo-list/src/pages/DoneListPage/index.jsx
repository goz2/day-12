import {useSelector} from 'react-redux'
import {useNavigate} from 'react-router-dom'
import '../../css/index.css'
import './index.css'
import {Table, Typography} from 'antd'

function DoneListPage() {
    const todoList = useSelector(state => state.todo.todoList).filter(todo => todo.done)
    const navigate = useNavigate()
    const handleClick = (id) => {
        navigate(`/done/${id}`)
    }
    const columns = [
        {
            title: 'Completed Todos',
            dataIndex: 'text',
            key: 'text',
            render: (text, record) => (
                <Typography.Text onClick={() => handleClick(record.id)} style={{cursor: 'pointer'}}>
                    {text}
                </Typography.Text>
            ),
        },
    ]
    return (
        <div className="curMain">
            {/*<List*/}
            {/*    header={<div className="flexMain">Completed Todos</div>}*/}
            {/*    dataSource={todoList}*/}
            {/*    renderItem={todo => (*/}
            {/*        <List.Item onClick={() => handleClick(todo.id)} className="curMain">*/}
            {/*            <Typography.Text>{todo.text}</Typography.Text>*/}
            {/*        </List.Item>*/}
            {/*    )}*/}
            {/*/>*/}
            <div className="tableContainer">
                <Table
                    columns={columns}
                    dataSource={todoList}
                    pagination={false}
                />
            </div>
        </div>
    )
}

export default DoneListPage