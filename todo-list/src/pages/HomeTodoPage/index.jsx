import TodoList from '../../components/TodoList'
import '../../css/index.css'

function HomeTodoPage() {
    return (
        <div>
            <div className="main flexMain">
                <TodoList/>
            </div>
        </div>
    )
}

export default HomeTodoPage