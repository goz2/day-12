import {configureStore} from '@reduxjs/toolkit'
import listSlice from '../slices/todoListSlice'

export default configureStore({
    reducer: {
        todo: listSlice
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    }),
})