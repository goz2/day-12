import TodoGenetator from '../TodoGenerator'
import TodoItemGroup from '../TodoItemGroup'
import './index.css'
import {useEffect} from 'react'
import useTodo from '../../hooks/todo.hook'

function TodoList() {
    const {getTodoList} = useTodo()
    useEffect(() => {
        getTodoList()
    }, [])
    return (
        <div>
            <div className="headerMargin">Todo List</div>
            <TodoItemGroup/>
            <TodoGenetator/>
        </div>
    )
}

export default TodoList