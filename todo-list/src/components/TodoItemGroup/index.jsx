import {useSelector} from 'react-redux'
import useTodo from '../../hooks/todo.hook'
import {Table, Button, Modal, Input} from 'antd'
import {DeleteOutlined, EditOutlined} from '@ant-design/icons'
import {useState} from 'react'
import './index.css'

function TodoGroup() {
    const todoList = useSelector((state) => state.todo.todoList)
    const {editTodo, deleteTodo, editTodoText} = useTodo()
    const [editModalVisible, setEditModalVisible] = useState(false)
    const [editedTodo, setEditedTodo] = useState(null)
    const [editedText, setEditedText] = useState('')

    const handleEditModalOpen = (todo) => {
        setEditedTodo(todo)
        setEditedText(todo.text)
        setEditModalVisible(true)
    }

    const handleEditModalClose = () => {
        setEditModalVisible(false)
    }
    const handleEditSave = async () => {
        if (editedText && editedText.trim().length > 0) {
            await editTodoText(editedTodo.id, editedText);
            setEditModalVisible(false);
        }
    }
    const handleChangeStatus = async (id, done) => {
        await editTodo(id, !done)
    }

    const handleRemove = async (id) => {
        await deleteTodo(id)
    }

    const columns = [
        {
            title: 'Todo',
            dataIndex: 'text',
            key: 'text',
            render: (text, record) => (
                <span className={!record.done ? 'normal' : 'line'}
                      onClick={() => handleChangeStatus(record.id, record.done)}>
                    {text}
                </span>
            ),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (text, record) => (
                <div>
                    <Button type="primary" onClick={() => handleEditModalOpen(record)}>
                        <EditOutlined/>
                    </Button>
                    <Button type="primary" danger onClick={() => handleRemove(record.id)} className="buttonMargin">
                        <DeleteOutlined/>
                    </Button>
                </div>

            ),
        },
    ]

    const data = todoList.map((todo) => ({
        key: todo.id,
        id: todo.id,
        text: todo.text,
        done: todo.done,
        action: 'delete',
    }))
    return (
        <div>
            <Table columns={columns} dataSource={data} pagination={false}/>
            <Modal
                title="Edit Todo"
                visible={editModalVisible}
                onCancel={handleEditModalClose}
                onOk={handleEditSave}
            >
                <Input
                    type="text"
                    value={editedText}
                    onChange={(e) => setEditedText(e.target.value)}
                />
            </Modal>
        </div>
    )
}

export default TodoGroup