import {useState} from 'react'
import './index.css'
import useTodo from '../../hooks/todo.hook'
import {Input, Button} from 'antd'

function TodoGenetator() {
    const [text, setText] = useState('')
    const {createTodo} = useTodo()
    const handleAdd = async () => {
        if (text && text.trim().length > 0) {
            await createTodo(text)
            setText('')
        }
    }
    return (
        <div className="contentMain">
            <Input type="text" onChange={e => setText(e.target.value)} value={text}/>
            <Button className="button" type="primary" onClick={handleAdd}>ADD</Button>

        </div>
    )
}

export default TodoGenetator