import axios from 'axios'

const requset = axios.create({
    baseURL: 'https://64c0b6690d8e251fd112642d.mockapi.io/api/'
})

export const getTodoList = () => {
    return requset.get('/todos')
}

export const createTodo = (text) => {
    return requset.post('/todos', {
        done: false,
        text
    })
}

export const deleteTodo = (id) => {
    return requset.delete(`/todos/${id}`)
}

export const editTodo = (id, done) => {
    return requset.put(`/todos/${id}`, {
        done
    })
}

export const editTodoText = (id, text) => {
    return requset.put(`/todos/${id}`, {
        text
    })
}

const todoApi = {
    getTodoList,
    createTodo,
    deleteTodo,
    editTodo,
    editTodoText
}

export default todoApi
