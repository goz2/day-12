import {createSlice} from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoList: [],
    },
    reducers: {
        loadTodoList: (state, action) => {
            state.todoList = action.payload
        }
    },
})

export const {loadTodoList} = counterSlice.actions

export default counterSlice.reducer