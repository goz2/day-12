import {Outlet} from 'react-router-dom'
import Navigator from './components/Navigator'

function App() {
    return (
        <div className="main">
            <Navigator></Navigator>
            <Outlet></Outlet>
        </div>
    )
}

export default App
