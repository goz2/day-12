import React from 'react'
import ReactDOM from 'react-dom/client'
import store from './stores/store'
import {Provider} from 'react-redux'
import {RouterProvider} from 'react-router-dom'
import {router} from './routers'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <Provider store={store}>
        <RouterProvider router={router}> </RouterProvider>
    </Provider>
)
