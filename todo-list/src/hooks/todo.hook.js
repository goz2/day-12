import {useDispatch} from 'react-redux'
import {loadTodoList} from '../slices/todoListSlice'
import todoApi from '../apis/todo.api'

const useTodo = () => {
    const dispatch = useDispatch()

    const getTodoList = async () => {
        const {data} = await todoApi.getTodoList()
        dispatch(loadTodoList(data))
    }

    const createTodo = async (text) => {
        await todoApi.createTodo(text)
        await getTodoList()
    }

    const editTodo = async (id, done) => {
        await todoApi.editTodo(id, done)
        await getTodoList()
    }

    const deleteTodo = async (id) => {
        await todoApi.deleteTodo(id)
        await getTodoList()
    }

    const editTodoText = async (id, text) => {
        await todoApi.editTodoText(id, text)
        await getTodoList()
    }

    return {
        getTodoList,
        createTodo,
        editTodo,
        deleteTodo,
        editTodoText
    }
}

export default useTodo