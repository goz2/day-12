## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Learned about four main topics: React Router, Mock API, Hook functions, and Ant Design. The scenes that impressed me were the live coding examples where we implemented React Router for client-side routing and used Mock APIs to simulate backend responses.

- R (Reflective): Enthusiastic.

- I (Interpretive): Today's class was packed with valuable information for frontend development. React Router is essential for creating single-page applications with dynamic navigation, allowing us to handle routing efficiently. The concept of Mock API was fascinating as it helps us test our frontend components and UI even before the backend is fully developed. Hook functions in React are powerful tools for managing state and logic in functional components. Finally, Ant Design is a popular UI library that offers pre-designed components to create beautiful and responsive user interfaces.

- D (Decisional): I am excited to apply what I learned today in my upcoming projects. React Router will help me build more dynamic and interactive web applications with smooth navigation. I see great potential in using Mock APIs to prototype and test my UI even without a fully functional backend. Hook functions will become a crucial part of my toolkit for managing state and side effects in functional components effectively.